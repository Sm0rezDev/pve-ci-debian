#!/bin/bash

RND_PW=$(pwgen -ns 8 1)

echo "clear_text_pw: $RND_PW";
mkpasswd -m sha-512 -S $(pwgen -ns 16 1) $RND_PW