# Debian Clout-Init Image

variable "address" {
  type        = string
  description = "Address to http content."
}

variable "password" {
  type      = string
  sensitive = true
}

# Resource Settings for the VM Template. 
source "proxmox-iso" "ci-debian" {

  # No TLS certificate? Skip TLS validation.
  #insecure_skip_tls_verify = true

  # General Settings.
  node                 = "exodus"
  pool                 = "Templates"
  vm_id                = "1000"
  vm_name              = "ci-debian-template"
  template_description = "Debian 11 Cloud-Init"


  # OS ISO.
  iso_url          = "https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-11.6.0-amd64-netinst.iso"
  iso_checksum     = "224cd98011b9184e49f858a46096c6ff4894adff8945ce89b194541afdfd93b73b4666b0705234bd4dff42c0a914fdb6037dd0982efb5813e8a553d8e92e6f51"
  iso_storage_pool = "local"

  unmount_iso = true

  # System Settings.
  os   = "l26"
  bios = "ovmf"

  efi_config {
    efi_storage_pool  = "vm-data"
    pre_enrolled_keys = true
    efi_type          = "4m"
  }

  machine = "q35"

  qemu_agent = true

  cloud_init              = true
  cloud_init_storage_pool = "vm-data"

  # CPU Settings.
  cores    = 1
  sockets  = 1
  cpu_type = "host"

  # Memory Settings.
  memory = 1024

  # Disk Settings.
  scsi_controller = "virtio-scsi-pci"

  disks {
    type         = "virtio"
    storage_pool = "vm-data"
    disk_size    = "16G"
    format       = "qcow2"
  }

  # VM Network Settings.
  network_adapters {
    model    = "virtio"
    bridge   = "vmbr0"
    vlan_tag = "5"
  }


  # PACKER Boot Commands.
  boot_command = [
    "<wait><wait><wait>c<wait><wait><wait>",
    "linux /install.amd/vmlinuz ",
    "auto=true ",
    "url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/preseed.cfg ",
    "hostname=ci-debian ",
    "domain=weirdcompany.net ",
    "interface=auto ",
    "vga=788 noprompt quiet --<enter>",
    "initrd /install.amd/initrd.gz<enter>",
    "boot<enter>"
  ]
  boot      = "c"
  boot_wait = "10s"


  # PACKER Autoinstall Settings.
  http_directory    = "http"
  http_bind_address = "${var.address}"
  http_port_min     = 8802
  http_port_max     = 8802

  ssh_pty      = true
  ssh_username = "debian"
  ssh_password = "${var.password}"
  ssh_timeout  = "30m"
}

# Build Definition to create the VM Template.
build {

  name    = "ci-debian"
  sources = ["source.proxmox-iso.ci-debian"]

  # Provisioning VM Template for Cloud-Init Integration in Proxmox #1.
  provisioner "file" {
    source      = "files/99_pve.cfg"
    destination = "/tmp/99_pve.cfg"
  }

  # Provisioning VM Template for Cloud-Init Integration in Proxmox #2.
  provisioner "shell" {
    inline = ["echo '${var.password}' | sudo -S cp /tmp/99_pve.cfg /etc/cloud/cloud.cfg.d/99_pve.cfg"]
  }

  # Provisioning VM Template for Cloud-Init Integration in Proxmox #3.
  provisioner "shell" {
    inline = [
      "export pw='${var.password}'",
      "echo $pw | sudo -S rm -rf /etc/cloud/cloud.cfg.d/subiquity-disable-cloudinit-networking.cfg",
      "echo $pw | sudo -S rm -rf /etc/cloud/cloud.cfg.d/99-installer.cfg",
      "echo $pw | sudo -S cloud-init clean",
      "echo $pw | sudo -S truncate -s 0 /etc/machine-id",
      "echo $pw | sudo -S rm /var/lib/dbus/machine-id",
      "echo $pw | sudo -S ln -s /etc/machine-id /var/lib/dbus/machine-id"
    ]
  }

  # Provision VM with custom MOTD.
  provisioner "file" {
    source      = "files/11-my-motd"
    destination = "/tmp/11-my-motd"
  }

  provisioner "shell" {
    inline = [
      "export pw='${var.password}'",
      "echo $pw | sudo -S cp /tmp/11-my-motd /etc/update-motd.d/",
      "echo $pw | sudo -S chmod +x /etc/update-motd.d/11-my-motd",
      "rm /tmp/11-my-motd"
    ]
  }
}